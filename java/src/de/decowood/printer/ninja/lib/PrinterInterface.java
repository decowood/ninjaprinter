package de.decowood.printer.ninja.lib;

import de.decowood.printer.ninja.lib.document.DocumentInterface;

public interface PrinterInterface {
	/**
	 * @throws PrintException
	 */
	public void print() throws PrintException;

	/**
	 * @param document
	 */
	public PrinterInterface enqueue(DocumentInterface document);

	/**
	 * @param document
	 */
	public DocumentInterface dequeue(DocumentInterface document);
}
