package de.decowood.printer.ninja.lib;

import com.sun.pdfview.PDFFile;
import de.decowood.printer.ninja.lib.document.DocumentInterface;

import java.io.IOException;

public class PDFFileFactory {
    public PDFFile factory(DocumentInterface document) throws IOException {
        return new PDFFile(document.toByteBuffer());
    }
}
