package de.decowood.printer.ninja.lib.document;

public interface DocumentInterface {

	public String toRawString();

	public byte[] toBytes();

	public java.nio.ByteBuffer toByteBuffer();
}
